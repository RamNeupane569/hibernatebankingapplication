package com.cerotid.hibernate.demo.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Scanner;

/*
 2. Modify Account class
    2.1 accountType data type to AccountType
    2.2 Add variable - 
		accountOpenDate, accountCloseDate, amount, accountNumber (Generate Randomly/ look UUID concept)

    2.3 Add behavior-
		- sendMoney()
			- Provide option to choose account
			- provide option to choose type of Transaction to initiate
			- Complete the transaction
 
 
 */
public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1174083424080945640L;// keep track of object version
	private AccountType accountType;// instance variable
	private Date accountOpenDate;
	private Date accountCloseDate;
	private double accountBalance;

	// generate accountNumber by system-- look into UUID
	private int accountNumber;

	public Account() {
		super();
	}

	public Account(AccountType accountType, Date accountOpenDate, double amount, int accountNumber) {
		super();
		this.accountType = accountType;
		this.accountOpenDate = accountOpenDate;
		this.accountBalance = amount;
		this.accountNumber = accountNumber;
	}

	public Account(AccountType accountType, Date accountOpenDate, double accountBalance) {
		this.accountType = accountType;
		this.accountOpenDate = accountOpenDate;
		this.accountBalance = accountBalance;
	}

	void sendMoney() {

	}

	public AccountType getCustomerChoiceAccount() {
		AccountType customerChoiceAccount = null;
		Integer accountType = getAccountTypeFromCustomer();

		boolean validInput;
		do {
			validInput = true;
			switch (accountType) {
			case 1:
				System.out.println("Checking account selected");
				customerChoiceAccount = AccountType.CHECKING;
				// validInput = true;
				break;
			case 2:
				System.out.println("Saving account selected");
				customerChoiceAccount = AccountType.SAVING;
				// validInput = true;
				break;
			case 3:
				System.out.println("Business_Checking selected");
				customerChoiceAccount = AccountType.BUSINESS_CHECKING;
				// validInput = true;
				break;
			default:
				System.out.println("Invalid choice! Please choose again!");
				// validInput = false;
			}
		} while (!validInput);

		return customerChoiceAccount;
	}

	public Integer getAccountTypeFromCustomer() {
		System.out.println("please enter an account type (1.checking \n2. saving \n3. business_checking)");
		Scanner userSelectedAccountType = new Scanner(System.in);
		return userSelectedAccountType.nextInt();
	}

	public AccountType getAccountType() {
		return accountType; // here just the instance variable not the local variable, no need of this
							// keyword
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType; // to give the priority to instance variable, use this keyword
	}

	public Date getAccountOpenDate() {
		return accountOpenDate;
	}

	public void setAccountOpenDate(Date accountOpenDate) {
		this.accountOpenDate = accountOpenDate;
	}

	public Date getAccountCloseDate() {
		return accountCloseDate;
	}

	public void setAccountCloseDate(Date accountCloseDate) {
		this.accountCloseDate = accountCloseDate;
	}

	public double getAmount() {
		return accountBalance;
	}

	public void setAmount(double amount) {
		this.accountBalance = amount;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	void printAccountInfo() {
		System.out.println("print account info");

	}

	@Override
	public String toString() {
		return "Account [accountType=" + accountType + ", accountOpenDate=" + accountOpenDate + ", accountCloseDate="
				+ accountCloseDate + ", accountBalance=" + accountBalance + ", accountNumber=" + accountNumber + "]";
	}

}
