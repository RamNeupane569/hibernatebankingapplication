package com.cerotid.hibernate.demo.entity;

import java.io.Serializable;

/*
 Create enum AccountType
 Enum Values - Checking, Saving, Business_Checking

*/

public enum AccountType implements Serializable{
	CHECKING, SAVING, BUSINESS_CHECKING
}
