package com.cerotid.hibernate.demo.entity;

import java.io.Serializable;

public class Address implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1375804530037163032L;
	private String streetName;
	private String zipCode;
	private String city;
	private String stateCode;

	
	public Address() {
	}

	public Address(String streetName, String zipCode, String city, String stateCode) {
		super();
		this.streetName = streetName;
		this.zipCode = zipCode;
		this.city = city;
		this.stateCode = stateCode;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Address [streetName=" + streetName + ", zipCode=" + zipCode + ", city=" + city + ", stateCode="
				+ stateCode + "]";
	}

}
