package com.cerotid.hibernate.demo.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Customer implements Serializable {


	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	private static final long serialVersionUID = -6743341531508488849L;
	
	@Column(name="first_Name")
	private String firstName;
	
	@Column(name="last_Name")
	private String lastName;
	
	private ArrayList<Account> accounts;
	private Address address;
	private String ssn;
	
	public Customer() {
		
	}

	public Customer(String firstName, String lastName, Address address, String ssn) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.ssn = ssn;
		this.accounts = new ArrayList<>();
	}

	/*public Customer() {
		// TODO Auto-generated constructor stub
		this.accounts = new ArrayList<>();
	}*/

	public Customer(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.accounts = new ArrayList<>();
	}

	public void printcustomerAccount() {
		System.out.println("print customer account information");

		for (Account account : this.accounts) {
			System.out.println("Account Type:" + account.getAccountType().toString());
			System.out.println("Account Balance:" + account.getAccountNumber());
		}
	}

	public String customerDetails(String ssn) {

		// search in your customer database to locate the customer

		System.out.println("print customer details");
		return firstName;

	}

	public void addAccount(Account account) {
		if(this.accounts == null) {
			this.accounts = new ArrayList<>();
		}
		this.accounts.add(account);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	@Override
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", accounts=" + accounts + ", address="
				+ address + ", ssn=" + ssn + "]";
	}

	public static Comparator<Customer> LastNameComparator = new Comparator<Customer>() {

		@Override
		public int compare(Customer o1, Customer o2) {
			if (o1.getLastName().compareTo(o2.getLastName()) == 0) {
				return o1.getFirstName().compareTo(o2.getFirstName());
			}

			return o1.getLastName().compareTo(o2.getLastName());
		}

	};

}