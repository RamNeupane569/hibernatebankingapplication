package com.cerotid.hibernate.demo;
//using Maven project
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.cerotid.hibernate.demo.entity.Customer;

public class AddCustomerDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Customer.class)
				.buildSessionFactory();

		// create session
		Session session = factory.getCurrentSession();
		
		try {
			//create customer object
			System.out.println("Creating new student object....");
			Customer customer = new Customer("Ram", "Neupane");
			
			//start transaction 
			session.beginTransaction();
			
			//save the customer object
			System.out.println("Saving customer object....");
			session.save(customer);
			
			//commit transaction 
			session.getTransaction().commit();
			
			System.out.println("done!!");
			
			
		}
		catch (Exception exc) {
			exc.printStackTrace();
			
		}
		finally {
			factory.close();
		}
	}
}
